'use strict';

angular.module('clientApp', [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ngMaterial',
        'LocalStorageModule',
        'angular-loading-bar',
        'ngMessages',
        'underscore',
        

        'app.config',
        'mod.nbos',
        'mod.idn',
        'mod.m{{moduleId}}',
])

angular.module('clientApp')
.constant('CLIENT_CONFIG',{
        CLIENT_ID: '{{clientId}}',
        CLIENT_SECRET: '',
        CLIENT_DOMAIN : 'http://localhost:9001'
})
