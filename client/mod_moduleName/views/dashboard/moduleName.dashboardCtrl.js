'use strict';

angular.module('mod.m{{moduleId}}')
.controller('{{moduleName}}DashboardCtrl', ['$scope', 'UserService', function($scope, UserService){

    $scope.message = "Hello from you module Dashboard";
    
    var init = function(){
        
    };

    init();
}]);