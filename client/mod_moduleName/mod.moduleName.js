angular.module('mod.m{{moduleId}}', []);

angular.module('mod.m{{moduleId}}')
  .constant('MOD_{{moduleName}}', {
        API_URL: '',
        API_URL_DEV: ''
    })
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m{{moduleId}}', {
                url: '/{{moduleName}}',
                parent: 'layout',
                template: "<div ui-view></div>",
                data: {
                    type: 'home'
                }
            })
            .state('m{{moduleId}}.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_{{moduleName}}/views/menu/{{moduleName}}.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "{{moduleName}}"
               }
           })
            .state('m{{moduleId}}.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_{{moduleName}}/views/dashboard/{{moduleName}}.dashboard.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dashboard",
                    module: "{{moduleName}}"
                }

            })

            //FOR APP ADMIN
           .state('m{{moduleId}}.admin', {
               url: '/admin',
               templateUrl: "mod_{{moduleName}}/views/admin/{{moduleName}}.admin.html",
               data: {
                   type: 'home',
                   menu: true,
                   admin : true,
                   disabled : false,
                   name: "Admin Page",
                   module: "{{moduleName}}"
               }
           })

    }])